﻿using Jivago.Controllers;
using Jivago.Data.Business;
using Jivago.Model;
using Moq;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using System.Net;
using System.Web.Http.Controllers;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Jivago.Tests.Controllers
{
    public class MoviesControllerTests 
    {
        private int quantidadeTotalFilmes;
     
        public MoviesControllerTests()
        {
            this.DataUOWMock = new Mock<IDataUnityOfWork>();
            this.MoviesRepositoryMock = new Mock<IMoviesRepository>();
            this.quantidadeTotalFilmes = 10;

            MoviesRepositoryMock.Setup<List<Movie>>(m => m.ObterTodos()).Returns(this.ListaDosFilmes());

            DataUOWMock.Setup(d => d.Movies).Returns(MoviesRepositoryMock.Object);
        }

        private List<Movie> ListaDosFilmes()
        {
            var filmes = Enumerable.Repeat(new Movie() { Id = 1 }, quantidadeTotalFilmes);
            return filmes.ToList();
        }

        [Fact]
        public void consegue_obter_todos_os_filmes()
        {
            //Arrange
            var controller = new MoviesController(DataUOWMock.Object);
            //Act
            var moveis = controller.Get();
            //Assert
            moveis.Should().HaveCount(quantidadeTotalFilmes);
        }

        [Fact]
        public void consegue_obter_um_filme_pelo_id()
        {
            //Arrange
            var id = 1;
            var controller = new MoviesController(DataUOWMock.Object);

            SetupRequestToController(controller, HttpMethod.Get, "");
            MoviesRepositoryMock.Setup(m => m.ObterPeloId(id)).Returns(new Movie() { Id = id });

            //Act
            var resposta = controller.Get(id);

            //Assert
            MoviesRepositoryMock.Verify<Movie>(m => m.ObterPeloId(id), Times.Once());
            resposta.Content.ReadAsAsync<Movie>().Result.Should().NotBeNull();
            resposta.Content.ReadAsAsync<Movie>().Result.Id.Should().Be(1);
        }

        [Fact]
        public void se_o_filme_requisitado_nao_existir_retorna_erro()
        {
            //Arrange
            var controller = new MoviesController(DataUOWMock.Object);
            SetupRequestToController(controller, HttpMethod.Get, "");
            var id = 2;
            Movie nullMovie = null;

            MoviesRepositoryMock.Setup<Movie>(m => m.ObterPeloId(id)).Returns(nullMovie);
            //Act
            var resposta = controller.Get(id);
            //Assert
            resposta.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public void se_existirem_modelerrors_nao_salva_e_retorna_erro()
        {
            //Arrange
            var controller = new MoviesController(DataUOWMock.Object);
            SetupRequestToController(controller, HttpMethod.Post, "");
            var postedMovie = new Movie();
            //Act
            controller.ModelState.AddModelError("", "");
            var resposta = controller.Post(postedMovie);
            
            //Assert
            MoviesRepositoryMock.Verify(m => m.Novo(postedMovie), Times.Never());
            resposta.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public void se_o_repositorio_lancar_excecao_ao_salvar_retorna_erro()
        {
            //Arrange
            var controller = new MoviesController(DataUOWMock.Object);
            var postedMovie = new Movie();

            MoviesRepositoryMock.Setup(m => m.Novo(postedMovie)).Throws(new ValidationException());
            SetupRequestToController(controller, HttpMethod.Post, "");
            //Act
            var resposta = controller.Post(postedMovie);
            //Assert
            resposta.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public void nao_havendo_erros_salva_o_filme_e_retorna_ja_com_o_id() 
        {
            //Arrange
            var controller = new MoviesController(DataUOWMock.Object);
            var postedMovie = new Movie();
            SetupRequestToController(controller, HttpMethod.Post, "");

            MoviesRepositoryMock.Setup(m => m.Novo(postedMovie)).Callback(() => postedMovie.Id = 1);
            //Act
            var resposta = controller.Post(postedMovie);
            //Assert
            var newMovie = resposta.Content.ReadAsAsync<Movie>().Result;
            resposta.StatusCode.Should().Be(HttpStatusCode.Created);
            newMovie.Should().NotBeNull();
            newMovie.Id.Should().BeGreaterThan(0);
        }

        [Fact]
        public void se_filme_puder_ser_alugado_retorna_filme_com_estoque_atualizado()
        {
            //Arrange
            var controller = new MoviesController(DataUOWMock.Object);
            var requestedId = 1;
            SetupRequestToController(controller, HttpMethod.Post, "");

            MoviesRepositoryMock.Setup<Movie>(m => m.Alugar(requestedId)).Returns(new Movie() { Id = 1, Rented = 2 });
            //Act
            var resposta = controller.Alugar(requestedId);
            //Assert
            var rentedMovie = resposta.Content.ReadAsAsync<Movie>().Result;
            MoviesRepositoryMock.Verify<Movie>(m => m.Alugar(requestedId), Times.Once());
            resposta.StatusCode.Should().Be(HttpStatusCode.OK);
            rentedMovie.Rented.Should().BeGreaterOrEqualTo(0);
        }

        [Fact]
        public void se_o_filme_nao_puder_ser_alugado_retorna_erro()
        {
            //Arrange
            var controller = new MoviesController(DataUOWMock.Object);
            var requestedId = 2;
            SetupRequestToController(controller, HttpMethod.Post, "");

            MoviesRepositoryMock.Setup(m => m.Alugar(requestedId)).Throws(new InvalidOperationException());
            //Act
            var resposta = controller.Alugar(requestedId);
            //Assert
            resposta.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public void se_existir_um_filme_para_devolucao_retorna_filme_com_estoque_atualizado()
        {
            //Arrange
            var controller = new MoviesController(DataUOWMock.Object);
            var requestedId = 2;
            SetupRequestToController(controller, HttpMethod.Post, "");

            MoviesRepositoryMock.Setup<Movie>(m => m.Devolver(requestedId)).Returns(new Movie() { Id = 1, Stock = 2 });
            //Act
            var resposta = controller.Devolver(requestedId);
            //Assert
            var movie = resposta.Content.ReadAsAsync<Movie>().Result;
            MoviesRepositoryMock.Verify<Movie>(m => m.Devolver(requestedId), Times.Once());
            resposta.StatusCode.Should().Be(HttpStatusCode.OK);
            movie.Rented.Should().BeGreaterOrEqualTo(0);
        
        }

        [Fact]
        public void se_o_filme_nao_precisar_ser_devolvido_retorna_erro_com_mensagem()
        {
            //Arrange
            var controller = new MoviesController(DataUOWMock.Object);
            var requestedId = 2;
            SetupRequestToController(controller, HttpMethod.Post, "");

            MoviesRepositoryMock.Setup(m => m.Devolver(requestedId)).Throws(new InvalidOperationException());
            //Act
            var resposta = controller.Devolver(requestedId);
            //Assert
            resposta.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }



        private static void SetupRequestToController(ApiController controller,HttpMethod method, string url)
        {
            var config = new HttpConfiguration();
            var request = new HttpRequestMessage(method, url);
            var route = config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}");
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "movies" } });

            controller.ControllerContext = new HttpControllerContext(config, routeData, request);
            controller.Request = request;
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
        }

        public Mock<IDataUnityOfWork> DataUOWMock { get; set; }

        public Mock<IMoviesRepository> MoviesRepositoryMock { get; set; }
    }
}
