﻿using Jivago.Data.Business;
using Jivago.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jivago.Data
{
    public class MoviesRepository : IMoviesRepository
    {
        public MoviesRepository()
        {
            this.context = new JivagoDbContext();
        }

        public List<Movie> ObterTodos()
        {
            return context.Movies.ToList();
        }

        public JivagoDbContext context { get; set; }

        public void Novo(Movie movie)
        {
            movie.Rented = 0;
            if (!ExisteComMesmoNome(movie.Name))
            {
                context.Movies.Add(movie);
                context.SaveChanges();
            }
            else
                throw new InvalidOperationException();
        }

        private bool ExisteComMesmoNome(string p)
        {
            return context.Movies.FirstOrDefault(m => m.Name.Equals(p)) != null;
        }


        public Movie Alugar(int id)
        {
            var movie = context.Movies.Where(m => m.Id.Equals(id)).First();
            
            if (movie.Stock > 0 && movie.Rented < movie.Stock)
                movie.Rented += 1;
            else
                throw new InvalidOperationException();

            context.SaveChanges();
            return movie;
        }

        public Movie Devolver(int id)
        {
            var movie = context.Movies.Where(m => m.Id.Equals(id)).First();

            if (movie.Stock > 0 && movie.Rented > 0)
                movie.Rented -= 1;
            else
                throw new InvalidOperationException();

            context.SaveChanges();
            return movie;
        }


        public Movie ObterPeloId(int id)
        {
            return context.Movies.Where(m => m.Id.Equals(id)).FirstOrDefault();
        }
    }
}
