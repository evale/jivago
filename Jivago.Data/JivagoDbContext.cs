﻿using Jivago.Data.SeedData;
using Jivago.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;

namespace Jivago.Data
{
    public class JivagoDbContext : DbContext
    {
        static JivagoDbContext()
        {
            Database.SetInitializer(new JivagoDatabaseInitializer());
        }

        public JivagoDbContext()
            : base(nameOrConnectionString: "Jivago") { }

        public DbSet<Movie> Movies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Movie>().Property(p => p.Name).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<Movie>().Property(p => p.Stock).IsRequired();
        }
    }
}
