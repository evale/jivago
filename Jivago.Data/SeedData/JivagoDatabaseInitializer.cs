﻿using Jivago.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Jivago.Data.SeedData
{
    public class JivagoDatabaseInitializer :
        DropCreateDatabaseAlways<JivagoDbContext>
    {
        public List<Movie> todosOsFilmes()
        {
            return new List<Movie>(){ new Model.Movie()
                {
                    Name = "Jivago",
                    Description = " Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
                    ImageUrl = "http://blog.ctnews.com/meyers/files/2010/05/zhivago5.jpg",
                    Stock = 3
                }
            };
        }
        protected override void Seed(JivagoDbContext context)
        {
            todosOsFilmes().ForEach(m => context.Movies.Add(m));
            context.SaveChanges();
        }
    }
}
