﻿using Jivago.Data.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jivago.Data
{
    public class DataUnityOfWork : IDataUnityOfWork
    {
        public IMoviesRepository Movies
        {
            get { return new MoviesRepository(); }
        }
    }
}
