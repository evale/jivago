﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jivago.Model
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Rented { get; set; }
        public int Stock { get; set; }
        public string ImageUrl { get; set; }
    }
}
