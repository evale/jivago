﻿using Jivago.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jivago.Data.Business
{
    public interface IMoviesRepository
    {
        List<Movie> ObterTodos();

        void Novo(Movie movie);

        Movie Alugar(int id);

        Movie Devolver(int id);

        Movie ObterPeloId(int id);
    }
}
