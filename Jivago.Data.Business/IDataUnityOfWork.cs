﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jivago.Data.Business
{
    public interface IDataUnityOfWork
    {
        IMoviesRepository Movies { get; }
    }
}
