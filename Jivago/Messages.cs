﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jivago
{
    public class Messages
    {
        public static readonly string MovieNotFound = "O filme solicitado nao foi encontrado";
        public static string InvalidModel = "O registro a tentar ser incluido e invalido";
    }
}