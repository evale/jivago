﻿using Jivago.Data.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Jivago.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(IDataUnityOfWork dataUOW)
        {
            this.dataUOW = dataUOW;
        }


        public ActionResult Index()
        {
            return View();
        }

        public IDataUnityOfWork dataUOW { get; set; }
    }
}
