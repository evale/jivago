﻿using Jivago.Data.Business;
using Jivago.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Jivago.Controllers
{
    public class MoviesController : ApiController
    {

        public MoviesController(IDataUnityOfWork dataUnityOfWork)
        {
            this.dataUnityOfWork = dataUnityOfWork;
        }

        // GET api/movies
        public IEnumerable<Movie> Get()
        {
            return this.dataUnityOfWork.Movies.ObterTodos();
        }

        // GET api/movies/5
        public HttpResponseMessage Get(int id)
        {
            var movie = this.dataUnityOfWork.Movies.ObterPeloId(id);
            
            if(movie != null)
                return Request.CreateResponse(HttpStatusCode.Created, movie);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound, Messages.MovieNotFound);
        }

        // POST api/movies
        public HttpResponseMessage Post(Movie movie)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    this.dataUnityOfWork.Movies.Novo(movie);

                    return Request.CreateResponse(HttpStatusCode.Created, movie);
                }
                catch (ValidationException exception)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, Messages.InvalidModel);
        }


        [HttpPut]
        [ActionName("Alugar")]
        public HttpResponseMessage Alugar(int id)
        {
            try
            {
                var movie = this.dataUnityOfWork.Movies.Alugar(id);
                return Request.CreateResponse(HttpStatusCode.OK, movie);
            } 
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpPut]
        [ActionName("Devolver")]
        public HttpResponseMessage Devolver(int id)
        {
            try
            {
                var movie = this.dataUnityOfWork.Movies.Devolver(id);
                return Request.CreateResponse(HttpStatusCode.OK, movie);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }


        public IDataUnityOfWork dataUnityOfWork { get; set; }
    }
}
