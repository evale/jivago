﻿using System.Web;
using System.Web.Optimization;
using Jivago.App_Start;

namespace Jivago
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/libs")
                .Include("~/Scripts/libs/jquery-1.7.2.js", "~/Scripts/libs/underscore.js", "~/Scripts/libs/backbone.js"));

            bundles.Add(new Bundle("~/Content/Less", new LessTransform(), new CssMinify())
                .Include("~/Content/less/reset.less")
                .Include("~/Content/less/global.less"));

        }
    }
}