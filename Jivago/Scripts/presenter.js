﻿// Filename: router.js
define([
  'jquery'
], function ($) {

    var toggleLoading = function (enabled) {
        if (enabled)
            $("#page-loading").addClass("active");
        else 
            setTimeout(function () { $("#page-loading").removeClass("active") }, 300);
    };
      
    return {
        toggleLoading: toggleLoading
    };
});
