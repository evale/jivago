define([
  'jquery',
  'underscore',
  'backbone',
  'collections/movies/MoviesCollection',
  'models/movie/MovieModel',
  'views/movies/MovieView',
  'presenter'
], function ($, _, Backbone, MoviesCollection, MovieModel, MovieView, presenter) {

  var MovieListView = Backbone.View.extend({
    el: $("#movielist-view"),
    listElement: $("#movielist"),
    events: {
        "click #btNew": "createNew"
    },
    initialize: function () {
        var that = this;
        that.collection = new MoviesCollection();
        that.collection.on('add', this.addOne, this);
        that.collection.on('reset', this.renderAll, this);
        that.collection.fetch({ dataType: "json" });
    },
    render: function () {
    },
    renderAll: function () {

        var mainElement = this.listElement.html('');

        _.each(this.collection.models, function (movie) {

            var view = new MovieView({ model: movie });

            $(mainElement).append(view.render().el);
        });
    },
    createNew: function () {

        $('#movie-form').parsley('validate');

        if (!$("#movie-form .parsley-error").length) {

            var movieJson = $("#movie-form").getJSON(true);
            var newMovie = new MovieModel(movieJson);

            presenter.toggleLoading(true);

            newMovie.save({}, { success: function () { this.addOne; presenter.toggleLoading(false);  } });
        }

    },
    addOne: function (movie) {

        var view = new MovieView({ model: movie });
        $("#movielist").append(view.render().el);

    }
  });

  return MovieListView;
  
});
