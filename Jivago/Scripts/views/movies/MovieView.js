﻿define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/movies/movieTemplate.html',
  'presenter'
], function ($, _, Backbone, movieTemplate, presenter) {

    var MovieView = Backbone.View.extend({

        tagName: "li",
        events: {
            "click .btAlugar": "alugar",
            "click .btDevolver": "devolver"
        },
        alugar: function () {
            presenter.toggleLoading(true);
            this.model.alugar(function () { presenter.toggleLoading(false); });
        },
        devolver: function () {
            presenter.toggleLoading(true);
            this.model.devolver(function () { presenter.toggleLoading(false); });
        },
        initialize: function () {
            this.model.on("change", this.render, this);
        },
        render: function () {
            var teste = this.model;
            var compiled = _.template(movieTemplate, { movie: this.model });
            this.$el.html(compiled);
            this.$el.find(".btAlugar").prop('disabled', !this.podeAlugar(this.model));
            this.$el.find(".btDevolver").prop('disabled', !this.podeDevolver(this.model));
            return this;
        },
        podeAlugar: function(model){
            return model.get('stock') - model.get('rented') > 0;
        },
        podeDevolver: function (model) {
            return model.get('rented') > 0;
        }
    });

    return MovieView;

});
