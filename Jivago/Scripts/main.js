﻿// Author: Thomas Davis <thomasalwyndavis@gmail.com>
// Filename: main.js

require.config({
  paths: {
      jquery: 'libs/jquery/jquery-min',
      jsonform: 'libs/jsonform',
      underscore: 'libs/underscore/underscore-min',
      backbone: 'libs/backbone/backbone-min',
      templates: '../templates'
  },
  shim: {
      'jsonform': ['jquery'],
      'libs/parsley.min': ['jquery'],
      'i18n/messages.pt_br': ['jquery']
  }
});

require(['jsonform', 'libs/parsley.min', 'i18n/messages.pt_br'])
require(['app'],
function (App) {
  App.initialize();
});
