﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/movie/MovieModel'
], function ($, _, Backbone, MovieModel) {
    var MoviesCollection = Backbone.Collection.extend({
        model: MovieModel,
        url: 'api/Movies/'
    });

    return MoviesCollection;
});
