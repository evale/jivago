// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'views/movies/MovieListView'
], function ($, _, Backbone, MovieListView) {
  
  var AppRouter = Backbone.Router.extend({
    routes: {
      
      // Default
      '*actions': 'defaultAction'
    }
  });
  
  var initialize = function(){

    var app_router = new AppRouter;

    app_router.on('route:defaultAction', function (actions) {
        var moviesView = new MovieListView();
        moviesView.render();
    });


    Backbone.history.start();
  };
  return { 
    initialize: initialize
  };
});
