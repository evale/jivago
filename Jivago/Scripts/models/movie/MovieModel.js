﻿define([  'underscore',  'backbone',], function (_, Backbone) {
    var MovieModel = Backbone.Model.extend({
        url: function () {
            var base = 'api/Movies/';            if (this.isNew())                return base;            return base + (base.charAt(base.length - 1) == '/' ? '' : '/') + this.id;
        },        defaults: {
            name: null,            description: null,            stock: 0,            rented: 0,            imageUrl: null
        },
        methodToURL: {
            'read': 'api/Movies/',
            'create': '/user/create',
            'update': '/user/update',
            'delete': '/user/remove'
        },
        alugar: function (callback) {
            this.save({}, { action: "alugar", success: callback });
        },
        devolver: function (callback) {
            this.save({}, { action: "devolver", success: callback });
        },
        sync: function (method, model, options) {
            if (options.action)
                options.url = "api/Movies/" + options.action + "/" + this.id;

            Backbone.sync(method, model, options);
        }

    });    return MovieModel;
});